import { IReceta } from "../models/ireceta.interface";

export const RecetasMock: IReceta[] = [
  {
    nombre: 'Cocido Madrileño',
    descripcion: 'Plato típico de Madrid',
    tiempo: 2
  },
  {
    nombre: 'Arroz al Horno',
    descripcion: 'Plato típico de Valencia',
    tiempo: 1
  }
]
