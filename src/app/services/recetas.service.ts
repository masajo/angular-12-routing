import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { RecetasMock } from '../mocks/recetas.mock';
import { IReceta } from '../models/ireceta.interface';

// Imports para trabajar con FIREBASE
import { collection, Firestore, collectionData, DocumentData, CollectionReference, DocumentReference } from '@angular/fire/firestore';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';


@Injectable({
  providedIn: 'root'
})
export class RecetasService {


  // Definimos nuestra colección de recetas que sacaremos de Firebase FireStore
  private recetasCollection: AngularFirestoreCollection<IReceta>;

  // Lista de recetas
  private _recetas$: Observable<IReceta[]>;


  // Observable de la lista de recetas
  // private _recetas$: Observable<DocumentData[]>


  constructor(private _firestore: Firestore, private _db: AngularFirestore) {


    // Coger la colecci¡ón "recetas" de Firestore
    // const coleccion = collection(this._firestore, 'recetas')

    // Inicializar la lista de recetas
    // this._recetas$ = collectionData(coleccion);

    // PRUEBA:
    this.recetasCollection = _db.collection<IReceta>('recetas');

    // Automatic Bind with changes in collection
    this._recetas$ = this.recetasCollection.snapshotChanges().pipe(
      map(actions => {
          return actions.map( action => {
            const data = action.payload.doc.data(); // Los datos del documento (receta), sin el ID
            const id = action.payload.doc.id; // El ID de la receta
            return {id, ...data}; // {id, nombre, descripcion, tiempo} de cada documento
          });
      })
    );

  }

  // MOCK: Método para enviar la lista de recetas
  obtenerTodasLasRecetas(): Observable<IReceta[]>{
    return of(RecetasMock);
  }

  // Obetener recetas desde FireBase
  obtenerRecetasFireBase(): Observable<IReceta[]> {
    return this._recetas$;
  }

  // Obtener Receta por ID
  obtenerRecetaPorID(id: string): Observable<IReceta | undefined> {
    // si usamos this.recetasCollection!.método estamos controlando
    // la sposibilidad de que this.recetasCollection sea undefined y así controlamos el error
    // que daría acceder a un método de un elemento undefined
    return this.recetasCollection!.doc<IReceta>(id).valueChanges();
  }

  // Actualizar Receta
  // ? Cuando llame a este método no le voy a hacer .subscribe() si no que se hace uso de .then()
  /**
   * _servicio.actualizarReceta(receta, 1234)
    .then(
      () => {})
    .catch(
      (error) => { })
    .finally(
      () => {})
  */
  actualizarReceta(receta: IReceta, id: string): Promise<void> {
    return this.recetasCollection!.doc<IReceta>(id).update(receta);
  }

  // Crear una Receta
  crearReceta(receta: IReceta): any{
    return this.recetasCollection!.add(receta);
  }

  // Eliminar una Receta
  // ? Cuando llame a este método no le voy a hacer .subscribe() si no que se hace uso de .then()
  /**
   * _servicio.eliminarReceta(1234)
    .then(
      () => {})
    .catch(
      (error) => { })
    .finally(
      () => {})
  */
  eliminarReceta(id:string): Promise<void> {
    return this.recetasCollection!.doc<IReceta>(id).delete();
  }

}
