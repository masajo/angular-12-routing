import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { DetailPageComponent } from './pages/detail-page/detail-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';

// En routes definimos nuestras rutas para este módulo de enrutado
const routes: Routes = [
  {
    // localhost:4200/
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    // localhost:4200/login
    path: 'login',
    component: LoginPageComponent
  },
  {
    // localhost:4200/home
    path: 'home',
    component: HomePageComponent,
    canActivate: [AuthGuard]
  },
  {
    // localhost:4200/detail/{id} --> ID es un parámetro
    path: 'detail/:id',
    component: DetailPageComponent,
    canActivate: [AuthGuard]
  },
  {
    // localhost:4200/"paginaInexistente"
    path: '**',
    component: NotFoundPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
