import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IReceta } from 'src/app/models/ireceta.interface';
import { RecetasService } from 'src/app/services/recetas.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  listaRecetas: IReceta[] = [];

  constructor(private _recetasService: RecetasService, private _router: Router) { }

  ngOnInit(): void {
    // TODO: Descomentar para ver cómo cargar la lista de recetas MOCK para poder pintarla
    // this._recetasService.obtenerTodasLasRecetas().subscribe(
    //   (respuesta) => this.listaRecetas = respuesta,
    //   (error) => alert(`Ha ocurrido un error al cargar las recetas: ${error}`),
    //   () => console.log('Recetas cargadas con éxito')
    // )
    this._recetasService.obtenerRecetasFireBase().subscribe(
      (respuesta) => {
        console.log('RECETAS OBTENIDAS:', respuesta)
        this.listaRecetas = respuesta as IReceta[];
      },
      (error) => alert(`Ha ocurrido un error al cargar las recetas: ${error}`),
      () => console.log('Recetas cargadas con éxito')
    )
  }

  // Método para navegar a la página de detalle
  irADetalle(i: number) {
    this._router.navigate([`/detail/${i}`])
  }

}
