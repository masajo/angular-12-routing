import { Component, OnInit, Input } from '@angular/core';
import { IReceta } from 'src/app/models/ireceta.interface';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss']
})
export class DetalleComponent implements OnInit {

  @Input() receta: IReceta = {
    nombre: '',
    descripcion: '',
    tiempo: 0
  }

  constructor() { }

  ngOnInit(): void {
    console.log('RECETA INPUT:', this.receta) // 2
  }

}
