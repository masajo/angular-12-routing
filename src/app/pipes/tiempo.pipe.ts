import { Pipe, PipeTransform } from '@angular/core';
import { IReceta } from '../models/ireceta.interface';

@Pipe({
  name: 'tiempo'
})
export class TiempoPipe implements PipeTransform {

  transform(receta: IReceta, unidades: string): string {

    if (unidades == "horas") {
      return `Tiempo Requerido: ${receta.tiempo} horas `;
    } else if (unidades == "minutos") {
      return `Tiempo Requerido: ${receta.tiempo}' `;
    } else {
      return `Tiempo Requerido: ${receta.tiempo}'' `;
    }
  }
}
