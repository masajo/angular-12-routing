import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

// Firebase FireStore Config
import { AngularFireModule } from '@angular/fire/compat';
import { FirestoreModule } from '@angular/fire/firestore';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { provideFirebaseApp, getApp, initializeApp } from '@angular/fire/app';




import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { FormularioLoginComponent } from './components/formulario-login/formulario-login.component';
import { ListaComponent } from './components/lista/lista.component';
import { DetalleComponent } from './components/detalle/detalle.component';
import { DetailPageComponent } from './pages/detail-page/detail-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TiempoPipe } from './pipes/tiempo.pipe';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LoginPageComponent,
    NotFoundPageComponent,
    FormularioLoginComponent,
    ListaComponent,
    DetalleComponent,
    DetailPageComponent,
    TiempoPipe
  ],
  imports: [
    BrowserModule,
    // AppRoutingMOdule sirve para cargar las rutas de la aplicación
    AppRoutingModule,
    BrowserAnimationsModule,
    // HTTPClientModule --> Sirve para poder hacer peticiones HTTP a Endpoints Rest
    HttpClientModule,
    ReactiveFormsModule,
    // Módulos para trabajar con Firebase
    AngularFireModule.initializeApp(environment.firebaseConfig),
    FirestoreModule,
    provideFirebaseApp(() => initializeApp(environment.firebaseConfig)),
    provideFirestore(() => getFirestore()),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
