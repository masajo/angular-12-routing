import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IReceta } from 'src/app/models/ireceta.interface';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss']
})
export class DetailPageComponent implements OnInit {

  idReceta: string = '';
  receta: IReceta = {
    nombre: '',
    descripcion: '',
    tiempo: 0
  };
  cargandoReceta: boolean = true

  constructor(private _route: ActivatedRoute) { }

  ngOnInit():void {

    // Obtener los datos que se me pasan por paráametro en la ruta
    // Obtener el ID de los PARAMS
    this._route.params.subscribe(
      (params) => {
        console.log('PARAMS:', params); // 1
        if (params.id) {
          this.idReceta = params.id;
        }
      },
      (error) => alert(`Ha habido un error al cargar la receta: ${error}`),
      () => console.log(`ID de receta obtenido correctamente de la ruta: ${this.idReceta}`)
    );

    // Obtenemos la Receta que vamos a pintar --> QUERYPARAMS
    this._route.queryParams.subscribe(
      (params) => {
        console.log('QUERY PARAMS', params);
        if (params.nombre && params.tiempo) {
          this.receta = {
            nombre: params.nombre,
            descripcion: params?.descripcion,
            tiempo: params.tiempo
          }
          this.cargandoReceta = false;
        }
      },
      (error) => alert(`Ha habido un error al cargar la receta: ${error}`),
      () => console.log(`Receta obtenida correctamente de la ruta: ${this.receta}`)
    );
  }
}
