import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor( private _authService: AuthService, private _router: Router ) { }

  ngOnInit(): void {
  }

  // Botón para realizar logout desde el servicio
  logout() {
    this._authService.logout(); // hacemos que isLoggedIn pase a false desde el servicio
    this._router.navigate(["login"]) // navegamos a login
  }

}
