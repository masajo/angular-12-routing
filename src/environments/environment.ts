// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyClk2sl15ENpa30XN3vqcZ9ZczEfe1BxfM",
    authDomain: "misrecetas-274be.firebaseapp.com",
    projectId: "misrecetas-274be",
    storageBucket: "misrecetas-274be.appspot.com",
    messagingSenderId: "839630821434",
    appId: "1:839630821434:web:962c6b805efb256b803d03",
    measurementId: "G-6VVHHX7FSJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
